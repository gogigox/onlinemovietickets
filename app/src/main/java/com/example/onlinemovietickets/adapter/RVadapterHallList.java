package com.example.onlinemovietickets.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Hall;


import java.util.List;


public class RVadapterHallList extends RecyclerView.Adapter<RVadapterHallList.MyViewHolder> {


    private List<Hall> hallList;

    public RVadapterHallList.OnRVItemClick listenerHallList;

    public interface OnRVItemClick {
        void onRVItemclick(Hall hall);

    }

    public RVadapterHallList(List<Hall> hallList, OnRVItemClick listenerHallList) {
        this.hallList = hallList;
        this.listenerHallList = listenerHallList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvHallNumberWritten;
        TextView tvSeatsNumberWritten;
        TextView tvHallNumber;
        TextView tvSeatsNumber;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvSeatsNumberWritten = itemView.findViewById(R.id.rv_hall_written_number_of_seats);
            tvHallNumberWritten = itemView.findViewById(R.id.rv_hall_written_number);
            tvHallNumber = itemView.findViewById(R.id.tv_recycler_hall_number);
            tvSeatsNumber = itemView.findViewById(R.id.tv_recycler_hall_seats_number);

        }

        public void bind(final Hall hall, final RVadapterHallList.OnRVItemClick listener) {

            tvHallNumber.setText(hall.getNumber());
            tvSeatsNumber.setText(hall.getNumberOfSeats());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(hall);
                }
            });
        }
    }


    @NonNull
    @Override
    public RVadapterHallList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_hall_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RVadapterHallList.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(hallList.get(i), listenerHallList);
    }


    @Override
    public int getItemCount() {
        return hallList.size();
    }


    //dodavanje i refresh rv liste
    public void setNewData(List<Hall> hallList) {
        this.hallList.clear();
        this.hallList.addAll(hallList);
        notifyDataSetChanged();
    }



}
