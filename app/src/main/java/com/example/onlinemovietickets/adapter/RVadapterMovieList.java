package com.example.onlinemovietickets.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Movie;

import java.util.ArrayList;
import java.util.List;


public class RVadapterMovieList extends RecyclerView.Adapter<RVadapterMovieList.MyViewHolder> {


    private List<Movie> movieList;
    private List<Movie> displayedList;

    public OnRVItemClick listenerMovieList;

    public interface OnRVItemClick {
        void onRVItemclick(Movie movie);
    }


    public RVadapterMovieList(List<Movie> movieList, OnRVItemClick listenerMovieList) {
        this.movieList = movieList;
        this.listenerMovieList = listenerMovieList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvGenre;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_recycler_movie_title);
            tvGenre = itemView.findViewById(R.id.tv_recycler_movie_genre);

        }

        public void bind(final Movie movie, final OnRVItemClick listener) {

            tvTitle.setText(movie.getTitle());
            tvGenre.setText(movie.getGenre());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(movie);
                }
            });
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_movie_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(movieList.get(i), listenerMovieList);
    }


    @Override
    public int getItemCount() {
        return movieList.size();
    }


    //dodavanje i refresh rv liste
    public void setNewData(List<Movie> movieList) {
        this.movieList.clear();
        this.movieList.addAll(movieList);
        notifyDataSetChanged();
    }

}
