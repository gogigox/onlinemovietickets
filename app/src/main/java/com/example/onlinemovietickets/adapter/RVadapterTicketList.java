package com.example.onlinemovietickets.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Ticket;

import java.util.List;



public class RVadapterTicketList extends RecyclerView.Adapter<RVadapterTicketList.MyViewHolder> {


    private List<Ticket> ticketList;

    public RVadapterTicketList.OnRVItemClick listenerTicketList;

    public interface OnRVItemClick {
        void onRVItemclick(Ticket ticket);

    }

    public RVadapterTicketList(List<Ticket> ticketList, OnRVItemClick listenerTicketList) {
        this.ticketList = ticketList;
        this.listenerTicketList = listenerTicketList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvMovieTitleWritten;
        TextView tvTicketNumberWritten;
        TextView tvMovieTitle;
        TextView tvTicketNumber;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvMovieTitleWritten = itemView.findViewById(R.id.rv_ticket_written_movie);
            tvTicketNumberWritten = itemView.findViewById(R.id.rv_ticket_written_number);
            tvMovieTitle = itemView.findViewById(R.id.rv_ticket_movie_title);
            tvTicketNumber = itemView.findViewById(R.id.rv_ticket_number);

        }

        public void bind(final Ticket ticket, final RVadapterTicketList.OnRVItemClick listener) {

            tvMovieTitle.setText(ticket.getProjection().getMovie().getTitle());
            tvTicketNumber.setText(String.valueOf(ticket.getSeatNumber()));


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(ticket);
                }
            });
        }
    }

    @NonNull
    @Override
    public RVadapterTicketList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_ticket_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RVadapterTicketList.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(ticketList.get(i), listenerTicketList);
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }



}
