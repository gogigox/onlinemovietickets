package com.example.onlinemovietickets.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.Movie;

import java.util.List;



public class RVadapterAccountList extends RecyclerView.Adapter<RVadapterAccountList.MyViewHolder>{

    private List<Accounts> accountsList;

    public RVadapterAccountList.OnRVItemClick listenerAccountList;


    public interface OnRVItemClick {
        void onRVItemclick(Accounts accounts);
    }


    public RVadapterAccountList(List<Accounts> accountsList, OnRVItemClick listenerAccountList) {
        this.accountsList = accountsList;
        this.listenerAccountList = listenerAccountList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvSurname;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvName = itemView.findViewById(R.id.tv_recycler_account_name);
            tvSurname = itemView.findViewById(R.id.tv_recycler_account_surname);

        }

        public void bind(final Accounts accounts, final RVadapterAccountList.OnRVItemClick listener) {

            tvName.setText(accounts.getName());
            tvSurname.setText(accounts.getSurname());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(accounts);
                }
            });
        }
    }


    @NonNull
    @Override
    public RVadapterAccountList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_account_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RVadapterAccountList.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(accountsList.get(i), listenerAccountList);
    }


    @Override
    public int getItemCount() {
        return accountsList.size();
    }


}


