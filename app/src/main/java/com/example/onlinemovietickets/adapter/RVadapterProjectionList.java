package com.example.onlinemovietickets.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Projection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RVadapterProjectionList  extends RecyclerView.Adapter<RVadapterProjectionList.MyViewHolder> {




    private List<Projection> projectionList;

    public RVadapterProjectionList.OnRVItemClick listenerProjectionList;


    public interface OnRVItemClick {
        void onRVItemclick(Projection projection);
    }


    public RVadapterProjectionList(List<Projection> projectionList, OnRVItemClick listenerProjectionList) {
        this.projectionList = projectionList;
        this.listenerProjectionList = listenerProjectionList;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvMovie;
        TextView tvTime;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            tvMovie = itemView.findViewById(R.id.tv_recycler_projection_movie);
            tvTime = itemView.findViewById(R.id.tv_recycler_projection_time);

        }

        public void bind(final Projection projection, final OnRVItemClick listener) {


            tvMovie.setText(projection.getMovie().getTitle());


            Calendar calendar = Calendar.getInstance();
            calendar.setTime(projection.getTime());

            int month = calendar.get(Calendar.MONTH) + 1;
            String monthStr = String.valueOf(month);
            if(month <= 9){
                monthStr = 0 + monthStr;
            }

            int min = calendar.get(Calendar.MINUTE);
            String minutes = "";
            if (min <= 9) {
                minutes = '0' + String.valueOf(min);
            } else {
                minutes = String.valueOf(min);
            }

            int hrs = calendar.get(Calendar.HOUR_OF_DAY);
            String hours = "";
            if (hrs <= 9) {
                hours = '0' + String.valueOf(hrs);
            } else {
                hours = String.valueOf(hrs);
            }

            String datum = calendar.get(Calendar.DATE) + "-" + monthStr + "-" + calendar.get(Calendar.YEAR)
                    + " " + hours + ":" + minutes;

            tvTime.setText(datum);



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(projection);
                }
            });
        }
    }


    @NonNull
    @Override
    public RVadapterProjectionList.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.rv_projection_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RVadapterProjectionList.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(projectionList.get(i), listenerProjectionList);
    }


    @Override
    public int getItemCount() {
        return projectionList.size();
    }

    //dodavanje i refresh rv liste
    public void setNewData(List<Projection> projectionList) {
        this.projectionList.clear();
        this.projectionList.addAll(projectionList);
        notifyDataSetChanged();
    }


    public boolean isClickable = true;

}
