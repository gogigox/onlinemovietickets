package com.example.onlinemovietickets.db;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;




@DatabaseTable(tableName = Movie.TABLE_NAME_MOVIE)
public class Movie {

    protected static final String TABLE_NAME_MOVIE = "movie";

    protected static final String FIELD_ID = "id";
    protected static final String FIELD_TITLE = "title";
    protected static final String FIELD_GENRE = "genre";
    protected static final String FIELD_ACTORS = "actors";
    protected static final String FIELD_IMAGE = "image";



    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_TITLE)
    private String title;

    @DatabaseField(columnName = FIELD_GENRE)
    private String genre;

    @DatabaseField(columnName = FIELD_ACTORS)
    private String actors;

    @DatabaseField(columnName = FIELD_IMAGE)
    private String image;




    public Movie() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                '}';
    }
}
