package com.example.onlinemovietickets.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    //Dajemo ime bazi
    public static final String DATABASE_NAME = "movieTicket.db";

    //i pocetnu verziju baze. Obicno krece od 1
    private static final int DATABASE_VERSION = 5;

    private static final String TAG = "DatabaseHelper";

    private Dao<Accounts, Integer> accountsDao = null;
    private Dao<Hall, Integer> hallDao = null;
    private Dao<Movie, Integer> movieDao = null;
    private Dao<Projection, Integer> projectionDao = null;
    private Dao<Ticket, Integer> ticketDao = null;


    //Potrebno je dodati konstruktor zbog pravilne inicijalizacije biblioteke
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    //Prilikom kreiranja baze potrebno je da pozovemo odgovarajuce metode biblioteke
    //prilikom kreiranja moramo pozvati TableUtils.createTable za svaku tabelu koju imamo
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Accounts.class);
            TableUtils.createTable(connectionSource, Hall.class);
            TableUtils.createTable(connectionSource, Movie.class);
            TableUtils.createTable(connectionSource, Projection.class);
            TableUtils.createTable(connectionSource, Ticket.class);

            Log.d(TAG, "onCreate: CREATED TABLE " + database.toString());
        } catch (SQLException e) {

            Log.e(TAG, "onCreate: TABLE NOT CREATED " + e);
            throw new RuntimeException(e);
        }

    }

    //kada zelimo da izmenomo tabele, moramo pozvati TableUtils.dropTable za sve tabele koje imamo
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, Accounts.class, true);
            TableUtils.dropTable(connectionSource, Hall.class, true);
            TableUtils.dropTable(connectionSource, Movie.class, true);
            TableUtils.dropTable(connectionSource, Projection.class, true);
            TableUtils.dropTable(connectionSource, Ticket.class, true);

            onCreate(database, connectionSource);

            Log.d(TAG, "onUpgrade: TABLE UPGRADED " + database.toString());

        } catch (SQLException e) {
            Log.e(TAG, "onUpgrade: TABLE UPGRADE ERROR " + e);

            throw new RuntimeException(e);
        }

    }


    //jedan Dao objekat sa kojim komuniciramo. Ukoliko imamo vise tabela
    //potrebno je napraviti Dao objekat za svaku tabelu

    public Dao<Accounts, Integer> getAccountsDao() throws SQLException {
        if (accountsDao == null) {
            accountsDao = getDao(Accounts.class);
        }

        return accountsDao;
    }


    public Dao<Hall, Integer> getHallDao() throws SQLException {
        if (hallDao == null) {
            hallDao = getDao(Hall.class);
        }

        return hallDao;
    }


    public Dao<Movie, Integer> getMovieDao() throws SQLException {
        if (movieDao == null) {
            movieDao = getDao(Movie.class);
        }

        return movieDao;
    }


    public Dao<Projection, Integer> getProjectionDao() throws SQLException {
        if (projectionDao == null) {
            projectionDao = getDao(Projection.class);
        }

        return projectionDao;
    }


    public Dao<Ticket, Integer> getTicketDao() throws SQLException {
        if (ticketDao == null) {
            ticketDao = getDao(Ticket.class);
        }

        return ticketDao;
    }


    //obavezno prilikom zatvarnaj rada sa bazom osloboditi resurse
    @Override
    public void close() {
        accountsDao = null;
        hallDao = null;
        movieDao = null;
        projectionDao = null;
        ticketDao = null;

        super.close();
    }
}



