package com.example.onlinemovietickets.db;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;



@DatabaseTable(tableName = Ticket.TABLE_NAME_TICKET)
public class Ticket {

    protected static final String TABLE_NAME_TICKET ="ticket";

    protected static final String FIELD_ID = "id";
    protected static final String FIELD_PROJECTION = "projection";
    protected static final String FIELD_ACCOUNTS = "accounts";
    protected static final String FIELD_SEAT_NUMBER = "seatNumber";



    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_PROJECTION, foreign = true, foreignAutoRefresh = true)
    private Projection projection;

    @DatabaseField(columnName = FIELD_ACCOUNTS, foreign = true, foreignAutoRefresh = true)
    private Accounts accounts;

    @DatabaseField(columnName = FIELD_SEAT_NUMBER)
    private int seatNumber;




    public Ticket() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Projection getProjection() {
        return projection;
    }

    public void setProjection(Projection projection) {
        this.projection = projection;
    }

    public Accounts getAccounts() {
        return accounts;
    }

    public void setAccounts(Accounts accounts) {
        this.accounts = accounts;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "accounts =" + accounts +
                '}';
    }
}
