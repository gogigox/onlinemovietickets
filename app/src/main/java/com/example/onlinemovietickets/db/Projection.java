package com.example.onlinemovietickets.db;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;


@DatabaseTable(tableName = Projection.TABLE_NAME_PROJECTION)
public class Projection {


    protected static final String TABLE_NAME_PROJECTION = "projection";

    protected static final String FIELD_ID = "id";
    protected static final String FIELD_TICKET_PRICE = "ticketPrice";
    protected static final String FIELD_MOVIE = "movie";
    protected static final String FIELD_TIME = "time";
    protected static final String FIELD_HALL = "hall";


    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_TICKET_PRICE)
    private int ticketPrice;

    @DatabaseField(columnName = FIELD_MOVIE, foreign = true, foreignAutoRefresh = true)
    private Movie movie;

    @DatabaseField(columnName = FIELD_TIME)
    private Date time;

    @DatabaseField(columnName = FIELD_HALL, foreign = true, foreignAutoRefresh = true)
    private Hall hall;




    public Projection() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @Override
    public String toString() {
        return "Projection{" +
                "movie=" + movie +
                '}';
    }
}
