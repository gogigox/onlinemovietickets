package com.example.onlinemovietickets.db;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import java.util.Date;


@DatabaseTable(tableName = Accounts.TABLE_NAME_ACCOUNTS)
public class Accounts {

    protected static final String TABLE_NAME_ACCOUNTS = "accounts";

    protected static final String FIELD_ID = "id";
    protected static final String FIELD_NAME = "name";
    protected static final String FIELD_SURNAME = "surname";
    protected static final String FIELD_BIRTH_DATE = "birthDate";
    protected static final String FIELD_USER_NAME = "userName";
    protected static final String FIELD_PASSWORD = "password";
    protected static final String FIELD_IS_ADMIN = "isAdmin";


    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_NAME)
    private String name;

    @DatabaseField(columnName = FIELD_SURNAME)
    private String surname;

    @DatabaseField(columnName = FIELD_BIRTH_DATE)
    private Date birthDate;

    @DatabaseField(columnName = FIELD_USER_NAME, unique = true)
    private String userName;

    @DatabaseField(columnName = FIELD_PASSWORD)
    private String password;

    @DatabaseField(columnName = FIELD_IS_ADMIN)
    private int isAdmin;


    public Accounts() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return "Accounts{" +
                "userName='" + userName + '\'' +
                '}';
    }
}
