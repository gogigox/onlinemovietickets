package com.example.onlinemovietickets.db;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;



@DatabaseTable(tableName = Hall.TABLE_NAME_HALL)
public class Hall {

    protected static final String TABLE_NAME_HALL = "hall";

    protected static final String FIELD_ID = "id";
    protected static final String FIELD_NUMBER = "number";
    protected static final String FIELD_NUMBER_OF_SEATS = "numberOfSeats";


    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = FIELD_NUMBER)
    private String number;

    @DatabaseField(columnName = FIELD_NUMBER_OF_SEATS)
    private String numberOfSeats;



    public Hall() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(String numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "number=" + number +
                '}';
    }
}
