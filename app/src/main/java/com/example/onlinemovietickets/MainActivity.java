package com.example.onlinemovietickets;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Hall;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";


    private Hall hall;

    private Accounts accounts;

    private DatabaseHelper databaseHelper;

    private SharedPreferences preferences = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getSharedPreferences("com.example.onlinemovietickets", MODE_PRIVATE);


        createAdmin();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    private void createAdmin() {

        String bDate = "03-05-2000";
        String name = "Pera";
        String surname = "Peric";
        String password = "123";
        String userName = "123";

        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date dateObj = null;

        try {
            dateObj = curFormater.parse(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        accounts = new Accounts();
        accounts.setName(name);
        accounts.setSurname(surname);
        accounts.setBirthDate(dateObj);
        accounts.setPassword(password);
        accounts.setUserName(userName);
        accounts.setIsAdmin(1);


        try {
            getDatabaseHelper().getAccountsDao().create(accounts);

        } catch (SQLException e) {
            e.printStackTrace();

        }

    }


    @Override
    public void onResume() {
        super.onResume();

        //proverava se da li je prvo pokretanje-samo tada se setuju sale
        if (preferences.getBoolean("firstrun", true)) {

            insertHall("1", "40");
            insertHall("2", "50");
            insertHall("3", "60");


            preferences.edit().putBoolean("firstrun", false).apply();
        }
    }



    private void insertHall(String number, String numberOfSeats) {

        hall = new Hall();
        hall.setNumber(number);
        hall.setNumberOfSeats(numberOfSeats);


        try {
            getDatabaseHelper().getHallDao().create(hall);


        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getCause().getCause() instanceof SQLiteConstraintException) {

                return;
            }

        }

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
