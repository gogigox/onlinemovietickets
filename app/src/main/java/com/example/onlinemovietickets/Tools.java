package com.example.onlinemovietickets;

import java.util.ArrayList;
import java.util.List;

public class Tools {


    public static List<String> getMovieGenre() {
        List<String> genre = new ArrayList<>();
        genre.add("action");
        genre.add("comedy");
        genre.add("sci-fi");
        genre.add("adventure");
        genre.add("thriller");
        return genre;
    }


}
