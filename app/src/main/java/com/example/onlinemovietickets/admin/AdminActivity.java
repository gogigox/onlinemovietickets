package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.register.LoginActivity;


//prva activity koju vidi admin

public class AdminActivity extends AppCompatActivity {


    private Button btnMovieSection;
    private Button btnAccountSection;
    private Button btnProjections;
    private FloatingActionButton fabExit;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        btnMovieSection = findViewById(R.id.btn_movie_section);
        btnAccountSection = findViewById(R.id.btn_account_section);
        btnProjections = findViewById(R.id.btn_projections);
        fabExit = findViewById(R.id.activity_admin_fab);


        btnMovieSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminActivity.this, AdminMovieActivity.class);
                startActivity(intent);

            }
        });



        btnAccountSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminActivity.this, AdminAccountActivity.class);
                startActivity(intent);

            }
        });


        btnProjections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminActivity.this, AdminProjectionActivity.class);
                startActivity(intent);

            }
        });


        fabExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }

}
