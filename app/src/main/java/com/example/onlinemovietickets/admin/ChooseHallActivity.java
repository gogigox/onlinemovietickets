package com.example.onlinemovietickets.admin;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;


import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterHallList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Hall;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.HelpMethods;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.GenericRawResults;


import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ChooseHallActivity extends AppCompatActivity implements RVadapterHallList.OnRVItemClick {


    private static final String TAG = "ChooseHallActivity";


    private RecyclerView recyclerView;

    private RVadapterHallList rVadapterHallList;


    private EditText etMovieDate;
    private EditText etMovieTime;
    private EditText etTicketPrice;
    private Toolbar toolbar;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private DatabaseHelper databaseHelper;

    private Projection projection;

    private Movie movie;

    private HelpMethods hm;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_hall);


        etMovieTime = findViewById(R.id.et_movie_time);
        etMovieDate = findViewById(R.id.et_movie_date);
        etTicketPrice = findViewById(R.id.et_ticket_price);

        // listener za biranje vremena projekcije
        etMovieTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //instancira se calendar, trebaju nam min i hrs
                //time picker dialog pomocu kojeg se to bira i stave se 0 ako je br <= 9
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ChooseHallActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String minutes = "";
                        String hours = "";
                        if (selectedMinute <= 9) {
                            minutes = '0' + String.valueOf(selectedMinute);
                        } else {
                            minutes = String.valueOf(selectedMinute);
                        }
                        if (selectedHour <= 9) {
                            hours = '0' + String.valueOf(selectedHour);
                        } else {
                            hours = String.valueOf(selectedHour);
                        }
                        etMovieTime.setText(hours + ":" + minutes);
                    }
                }, hour, minute, true);// 24 h format
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        etMovieDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // helpMethods sluzi za biranje datuma
                hm = new HelpMethods();
                int[] datumNiz = hm.calendarPosition(etMovieDate.getText().toString());
                final int mDay = datumNiz[0];
                final int mMonth = datumNiz[1];
                final int mYear = datumNiz[2];

                DatePickerDialog datePickerDialog = new DatePickerDialog(ChooseHallActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                etMovieDate.setText(hm.createDateString(dayOfMonth, monthOfYear, year));
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();

            }
        });


        fabBack = findViewById(R.id.choose_hall_fab);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChooseHallActivity.this, ChooseMovieActivity.class);
                startActivity(intent);

            }
        });

        fabLogout = findViewById(R.id.activity_choose_hall_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChooseHallActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        setupToolbar();
        setupRV();

    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_hall_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterHallList == null) {

            try {
                rVadapterHallList = new RVadapterHallList(getDatabaseHelper().getHallDao().queryForAll(), this);
                recyclerView.setAdapter(rVadapterHallList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterHallList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Hall hall) {


        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");


        Date earlyDate = null;
        Date currentDate = new Date(); // datum za sadasnje vreme
        String sDate = etMovieDate.getText().toString(); //string za datum projekcije
        String sTime = etMovieTime.getText().toString(); //string za vreme projekcije
        String ticketPrice = etTicketPrice.getText().toString();


        int movieId = Integer.parseInt(getIntent().getStringExtra("MOVIE_ID"));


        try {
            movie = getDatabaseHelper().getMovieDao().queryForId(movieId);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //prosledili smo movie id pomocu kojeg setujemo movie u projekciju
        projection = new Projection();
        projection.setMovie(movie);


        String plus = sDate + " " + sTime; //string koji se dobije sabiranjem projekcijinog datuma i vremena
        //pa se to parsira u earlyDate-dobijemo datum(koji sadrzi datum i vreme projekcije)

        try {
            earlyDate = sdf.parse(plus);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (sDate.equals("") || sTime.equals("")) {
            Toast.makeText(ChooseHallActivity.this, "CHOOSE DATE / TIME", Toast.LENGTH_SHORT).show();


        } else if (earlyDate.compareTo(currentDate) < 0) {
            Toast.makeText(ChooseHallActivity.this, "THE DATE / TIME HAS PASSED", Toast.LENGTH_SHORT).show();


        } else if (ticketPrice.equals("")) {
            Toast.makeText(ChooseHallActivity.this, "TICKET PRICE INCORRECT", Toast.LENGTH_SHORT).show();


        } else {

            projection.setTime(earlyDate);//sadrzi datum i vreme projekcije

            projection.setTicketPrice(Integer.parseInt(ticketPrice));

            projection.setHall(hall);//setuje se hall iz onRVItemclick(Hall hall)

            try {

                //rastavljamo datum, odnosno string sDate koji je korisnik uneo na dan, mesec i godinu
                //da bi napravili string checkDate kojim cemo vrsiti upit nad bazom
                //projekcijino vreme sadrzi vreme i datum projekcije
                //pravimo upit nad bazom gde trazimo sve projekcije gde treba da je od projekcinijog vremena datum
                //isti sa onim koji je korisnik upisao i sale da su iste, dobijemo rawResults koji daje listu tih projekcija-listaP
                String[] datumNiz = sDate.split("-");
                final String mDay = datumNiz[0];
                final String mMonth = datumNiz[1];
                final String mYear = datumNiz[2];
                String checkDate = "'" + mYear + '-' + mMonth + '-' + mDay + "'";
                String query = "SELECT * FROM Projection WHERE Date(time) = " + checkDate + " and hall =" + hall.getId();
                GenericRawResults<Projection> rawResults =
                        getDatabaseHelper().getProjectionDao().queryRaw(query, getDatabaseHelper().getProjectionDao().getRawRowMapper());
                List<Projection> listaP = rawResults.getResults();

                //ako su datumi i sale iste za dve projekcije treba proveriti vreme pre nego sto kreiramo projekciju zbog preklapanja
                //uzeli smo da traje projekcija 2h, to su startTime i endTime, za projekciju koja se kreira
                Calendar cal = Calendar.getInstance(); // kreira calendar
                cal.setTime(projection.getTime()); // sets calendar time/date
                cal.add(Calendar.HOUR_OF_DAY, 2); // dodaje 2h, to je trajanje projekcije

                Date startTime = projection.getTime();
                Date endTime = cal.getTime();// vraca novi date objekat, 2h u budocnosti

                //timeStart i timeEnd su pocetak i kraj projekcije iz gorepomenute listeP
                Boolean provera = true;
                for (Projection p : listaP) {
                    Date timeStart = p.getTime();
                    cal.setTime(p.getTime()); // sets calendar time/date
                    cal.add(Calendar.HOUR_OF_DAY, 2);
                    Date timeEnd = cal.getTime();


                    //vrsi se provera da se nebi preklapalo vreme projekcije ako su datumi i sale iste
                    //npr. ako je trajanje 2h a jedna pocinje u 12h, ne moze se kreirati projekcija koja pocinje u 11h ili 13h
                    if (timeStart.compareTo(endTime) <= 0 && endTime.compareTo(timeEnd) <= 0 || timeStart.compareTo(startTime) <= 0
                            && startTime.compareTo(timeEnd) <= 0) {

                        provera = false;
                        break;
                    }
                }

                if (provera) {

                    try {

                        getDatabaseHelper().getProjectionDao().create(projection);

                    } catch (SQLException e) {
                        e.printStackTrace();
                        if (e.getCause().getCause() instanceof SQLiteConstraintException) {
                            return;
                        }
                    }

                    Toast.makeText(ChooseHallActivity.this, "projection created", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(ChooseHallActivity.this, AdminProjectionActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(ChooseHallActivity.this, "overlaping with other projection!", Toast.LENGTH_SHORT).show();

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}



