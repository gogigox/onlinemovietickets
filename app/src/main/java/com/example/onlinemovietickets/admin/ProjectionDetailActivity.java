package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.Calendar;


public class ProjectionDetailActivity extends AppCompatActivity {


    private DatabaseHelper databaseHelper;


    private TextView tvMovie;
    private TextView tvTime;
    private TextView tvHall;
    private TextView tvTicketPrice;
    private TextView tvNumOfTickets;
    private EditText etNumOfTickets;
    private TextView tvTotalPrice;
    private EditText etTotalPrice;

    private Projection projection;

    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projection_detail);


        getAndSetData(getIntent());

        btnDelete = findViewById(R.id.btn_projection_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteProjection();

                Toast.makeText(ProjectionDetailActivity.this, "projection deleted", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProjectionDetailActivity.this, AdminProjectionActivity.class);
                startActivity(intent);
            }
        });


        fabBack = findViewById(R.id.activity_projection_detail_fab);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProjectionDetailActivity.this, AdminProjectionActivity.class);
                startActivity(intent);
            }
        });

        fabLogout = findViewById(R.id.activity_projection_detail_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ProjectionDetailActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }


    private void getAndSetData(Intent intent) {
        int projection_id = Integer.parseInt(getIntent().getStringExtra("PROJECTION_ID"));

        try {
            projection = getDatabaseHelper().getProjectionDao().queryForId(projection_id);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        tvMovie = findViewById(R.id.tv_projection_detail_movie);
        tvTime = findViewById(R.id.tv_projection_detail_time);
        tvHall = findViewById(R.id.tv_projection_detail_hall);
        tvTicketPrice = findViewById(R.id.tv_projection_detail_ticket_price);
        tvNumOfTickets = findViewById(R.id.tv_num_of_tickets);
        tvNumOfTickets.setVisibility(View.GONE);
        etNumOfTickets = findViewById(R.id.et_num_of_tickets);
        etNumOfTickets.setVisibility(View.GONE);
        tvTotalPrice = findViewById(R.id.tv_total_price);
        tvTotalPrice.setVisibility(View.GONE);
        etTotalPrice = findViewById(R.id.et_total_price);
        etTotalPrice.setVisibility(View.GONE);


        //instanca calendara za prikaz vremena i datuma projekcije, mesecima ide + 1 jer krecu od 0
        //a dodaje se 0 ako je br <= 9
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(projection.getTime());

        int month = calendar.get(Calendar.MONTH) + 1;
        String monthStr = String.valueOf(month);
        if (month <= 9) {
            monthStr = 0 + monthStr;
        }

        int hrs = calendar.get(Calendar.HOUR_OF_DAY);
        String hours = "";
        if (hrs <= 9) {
            hours = '0' + String.valueOf(hrs);
        } else {
            hours = String.valueOf(hrs);
        }

        int min = calendar.get(Calendar.MINUTE);
        String minutes = "";
        if (min <= 9) {
            minutes = '0' + String.valueOf(min);
        } else {
            minutes = String.valueOf(min);

        }
        String datum = calendar.get(Calendar.DATE) + "-" + monthStr + "-" + calendar.get(Calendar.YEAR)
                + " " + hours + ":" + minutes;


        tvMovie.setText(projection.getMovie().getTitle());
        tvTime.setText(datum);
        tvHall.setText(projection.getHall().getNumber());
        tvTicketPrice.setText(Integer.toString(projection.getTicketPrice()));

    }


    private void deleteProjection() {

        try {
            getDatabaseHelper().getProjectionDao().delete(projection);
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }

}
