package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;

public class DeleteAccountActivity extends AppCompatActivity {


    private DatabaseHelper databaseHelper;

    private EditText et_name;
    private EditText et_surName;
    private EditText et_birthDate;
    private EditText et_password;
    private EditText et_userName;
    private Button btn_signup;
    private Button btnConfirm;
    private TextView tv_linkLogin;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private Accounts accounts;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        getAndSetData(getIntent());

        tv_linkLogin = (TextView)findViewById(R.id.link_login);
        tv_linkLogin.setVisibility(View.GONE);

        btn_signup = findViewById(R.id.btn_signup_create_account);
        btn_signup.setVisibility(View.GONE);


        btnConfirm = findViewById(R.id.btn_signup_confirm);
        btnConfirm.setText("delete account");
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAccount();

                    Toast.makeText(DeleteAccountActivity.this, "account deleted", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(DeleteAccountActivity.this, AdminAccountActivity.class);
                startActivity(intent);
            }
        });


        fabBack = findViewById(R.id.activity_signup_fab);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeleteAccountActivity.this, AdminAccountActivity.class);
                startActivity(intent);
            }
        });

        fabLogout = findViewById(R.id.activity_signup_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DeleteAccountActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


    }


    private void getAndSetData(Intent intent) {

        //prosledjen je accountov id da bi ga nasli iz baze
        //admin ne moze da menja accountove podatke vec samo da ga izbrise
        int acount_id = Integer.parseInt(getIntent().getStringExtra("ACCOUNT_ID"));

        try {
            accounts = getDatabaseHelper().getAccountsDao().queryForId(acount_id);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        et_name = findViewById(R.id.et_name);
        et_name.setFocusable(false);
        et_surName = findViewById(R.id.et_surname);
        et_surName.setFocusable(false);
        et_birthDate = findViewById(R.id.et_birthDate);
        et_birthDate.setFocusable(false);
        et_password = findViewById(R.id.et_signup_password);
        et_password.setFocusable(false);
        et_userName = findViewById(R.id.et_signup_userName);
        et_userName.setFocusable(false);

        et_name.setText(accounts.getName());
        et_surName.setText(accounts.getSurname());
        et_birthDate.setText(accounts.getBirthDate().toString());
        et_password.setText(accounts.getPassword());
        et_userName.setText(accounts.getUserName());
    }


    private void deleteAccount() {

        try {
            getDatabaseHelper().getAccountsDao().delete(accounts);
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
