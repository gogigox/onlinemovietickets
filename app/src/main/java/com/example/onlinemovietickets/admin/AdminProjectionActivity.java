package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterProjectionList;;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;


public class AdminProjectionActivity extends AppCompatActivity implements RVadapterProjectionList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterProjectionList rVadapterProjectionList;

    private Toolbar toolbar;
    private Button btnAddProjection;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private DatabaseHelper databaseHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_projections);

        btnAddProjection = findViewById(R.id.btn_add_projection);
        fabBack = findViewById(R.id.activity_admin_projection_fab);


        setupToolbar();
        setupRV();


        btnAddProjection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminProjectionActivity.this, ChooseMovieActivity.class);
                startActivity(intent);

            }
        });

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminProjectionActivity.this, AdminActivity.class);
                startActivity(intent);

            }
        });

        fabLogout = findViewById(R.id.activity_admin_projections_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminProjectionActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_projections_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterProjectionList == null) {

            try {
                rVadapterProjectionList = new RVadapterProjectionList(getDatabaseHelper().getProjectionDao().queryForAll(), this);
                recyclerView.setAdapter(rVadapterProjectionList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterProjectionList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Projection projection) {

        Intent intent = new Intent(AdminProjectionActivity.this, ProjectionDetailActivity.class);
        intent.putExtra("PROJECTION_ID",String.valueOf(projection.getId()));
        startActivity(intent);
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
