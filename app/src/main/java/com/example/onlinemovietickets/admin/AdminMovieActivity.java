package com.example.onlinemovietickets.admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterMovieList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;



//povezana je sa xml od activity_base !


public class AdminMovieActivity extends AppCompatActivity implements RVadapterMovieList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterMovieList rVadapterMovieList;


    private Toolbar toolbar;
    private Button btnAddMovie;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabMyAccount;
    private FloatingActionButton fabLogout;
    private TextView tvFabAccText;

    private DatabaseHelper databaseHelper;



    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        fabMyAccount = findViewById(R.id.activity_base_account_fab);
        fabMyAccount.setVisibility(View.GONE);

        tvFabAccText = findViewById(R.id.tv_activity_base_fabAcc_text);
        tvFabAccText.setVisibility(View.GONE);


        btnAddMovie = findViewById(R.id.btn_add_movie);

        fabBack = findViewById(R.id.activity_base_admin_fab);
        fabLogout = findViewById(R.id.activity_base_fab_logout);



        setupToolbar();
        setupRV();


        btnAddMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminMovieActivity.this, AddMovieActivity.class);
                startActivity(intent);

            }
        });



        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminMovieActivity.this, AdminActivity.class);
                startActivity(intent);

            }
        });


        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminMovieActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


    }


    private void setupRV(){
        recyclerView = findViewById(R.id.rv_movie_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }



    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(rVadapterMovieList == null) {

            try {
                rVadapterMovieList = new RVadapterMovieList(getDatabaseHelper().getMovieDao().queryForAll(), this);
                recyclerView.setAdapter(rVadapterMovieList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            rVadapterMovieList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Movie movie) {

        Intent intent = new Intent(AdminMovieActivity.this, DeleteMovieActivity.class);
        intent.putExtra("MOVIE_ID",String.valueOf(movie.getId()));
        startActivity(intent);


    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
