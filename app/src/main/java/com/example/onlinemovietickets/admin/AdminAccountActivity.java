package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterAccountList;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;


public class AdminAccountActivity extends AppCompatActivity implements RVadapterAccountList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterAccountList rVadapterAccountList;

    private Toolbar toolbar;
    private Button btnAddAccount;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private DatabaseHelper databaseHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_account);

        btnAddAccount = findViewById(R.id.btn_add_account);
        fabBack = findViewById(R.id.activity_admin_account_fab);


        setupToolbar();
        setupRV();


        btnAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminAccountActivity.this, AddAccountActivity.class);
                startActivity(intent);

            }
        });

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminAccountActivity.this, AdminActivity.class);
                startActivity(intent);

            }
        });

        fabLogout = findViewById(R.id.activity_admin_account_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AdminAccountActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }

    private void setupRV() {
        recyclerView = findViewById(R.id.rv_account_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(rVadapterAccountList == null) {

            try {
                rVadapterAccountList = new RVadapterAccountList(getDatabaseHelper().getAccountsDao().queryForAll(), this);
                recyclerView.setAdapter(rVadapterAccountList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            rVadapterAccountList.notifyDataSetChanged();
        }
    }

    @Override
    public void onRVItemclick(Accounts accounts) {

        // admin je zakljucan i njegovi podaci se mogu menjati samo preko koda
        if (accounts.getIsAdmin() == 1) {
            Toast.makeText(AdminAccountActivity.this, "LOCKED !", Toast.LENGTH_SHORT).show();

        } else {

            Intent intent = new Intent(AdminAccountActivity.this, DeleteAccountActivity.class);
            intent.putExtra("ACCOUNT_ID", String.valueOf(accounts.getId()));
            startActivity(intent);
        }

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
