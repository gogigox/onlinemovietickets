package com.example.onlinemovietickets.admin;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.Tools;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.List;


public class AddMovieActivity extends AppCompatActivity {


    private static final int SELECT_PICTURE = 1;

    private static final String TAG = "Dozvola";


    private RecyclerView recyclerView;

    private ImageButton ibAddMovieImage;
    private EditText etAddMovieTitle;
    private EditText etAddMovieGenre;
    private Spinner spMovieGenre;
    private EditText etAddMovieActors;
    private Button btnConfirm;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private String imagePath;

    private DatabaseHelper databaseHelper;

    private Movie movie;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);


        recyclerView = findViewById(R.id.rv_projections_list);
        recyclerView.setVisibility(View.GONE);


        ibAddMovieImage = findViewById(R.id.ib_add_movie);
        etAddMovieTitle = findViewById(R.id.et_add_movie_title);
        etAddMovieGenre = findViewById(R.id.et_add_movie_genre);
        etAddMovieGenre.setFocusable(false);
        spMovieGenre = findViewById(R.id.spinner);
        etAddMovieActors = findViewById(R.id.et_add_movie_actors);
        btnConfirm = findViewById(R.id.btn_add_movie_confirm);
        fabBack = findViewById(R.id.activity_add_movie_fab_back);
        fabLogout = findViewById(R.id.activity_add_movie_fab_logout);


        // setuje se spinnerAdapter da spiner prikazuje listu zanrova u klasi tools
        List<String> movieGenre = Tools.getMovieGenre();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, movieGenre);
        spMovieGenre.setAdapter(adapter);


        spMovieGenre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                etAddMovieGenre.setText(parent.getItemAtPosition(position).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddMovieActivity.this, AdminMovieActivity.class);
                startActivity(intent);

            }
        });


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add();

                Toast.makeText(AddMovieActivity.this, "movie added", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AddMovieActivity.this, AdminMovieActivity.class);
                startActivity(intent);


            }
        });

        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddMovieActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        ibAddMovieImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();

            }
        });

    }


    private void add() {

        movie = new Movie();
        movie.setTitle(etAddMovieTitle.getText().toString().trim());
        movie.setGenre(etAddMovieGenre.getText().toString().trim());
        movie.setActors(etAddMovieActors.getText().toString().trim());
        movie.setImage(imagePath);

        try {
            getDatabaseHelper().getMovieDao().create(movie);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

        }
    }


    private void selectPicture() {
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    imagePath = picturePath;
                    cursor.close();


                    ibAddMovieImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));


                    Toast.makeText(this, picturePath, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }

}
