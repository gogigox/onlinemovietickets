package com.example.onlinemovietickets.admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;


public class DeleteMovieActivity extends AppCompatActivity {


    private DatabaseHelper databaseHelper;

    private RecyclerView recyclerView;

    private TextView tvDelMovie;
    private ImageButton ibMovieDetail;
    private TextView tvTitleDetail;
    private TextView tvGenreDetail;
    private TextView tvActorsDetail;

    private Movie movie;

    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnDelete;

 //   public static final int notificationID = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);


        recyclerView = findViewById(R.id.rv_projections_list);
        recyclerView.setVisibility(View.GONE);

        tvDelMovie = findViewById(R.id.tv_add_movie);
        tvDelMovie.setVisibility(View.GONE);


        getAndSetData(getIntent());


        btnDelete = findViewById(R.id.btn_add_movie_confirm);
        btnDelete.setText("delete movie");
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteMovie();

                    Toast.makeText(DeleteMovieActivity.this, "movie deleted", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(DeleteMovieActivity.this, AdminMovieActivity.class);
                startActivity(intent);
            }
        });

        fabBack = findViewById(R.id.activity_add_movie_fab_back);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeleteMovieActivity.this, AdminMovieActivity.class);
                startActivity(intent);
            }
        });

        fabLogout = findViewById(R.id.activity_add_movie_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DeleteMovieActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }


    private void getAndSetData(Intent intent) {
        int movie_id = Integer.parseInt(getIntent().getStringExtra("MOVIE_ID"));

        try {
            movie = getDatabaseHelper().getMovieDao().queryForId(movie_id);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tvTitleDetail = findViewById(R.id.et_add_movie_title);
        tvTitleDetail.setFocusable(false);
        tvGenreDetail = findViewById(R.id.et_add_movie_genre);
        tvGenreDetail.setFocusable(false);
        tvActorsDetail = findViewById(R.id.et_add_movie_actors);
        tvActorsDetail.setFocusable(false);
        ibMovieDetail = findViewById(R.id.ib_add_movie);
        ibMovieDetail.setFocusable(false);
        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) ibMovieDetail.getLayoutParams();
        marginParams.setMargins(marginParams.leftMargin,
                80,
                marginParams.rightMargin,
                marginParams.bottomMargin);

        tvTitleDetail.setText(movie.getTitle());
        tvGenreDetail.setText(movie.getGenre());
        tvActorsDetail.setText(movie.getActors());
        ibMovieDetail.setImageBitmap(BitmapFactory.decodeFile(movie.getImage()));

    }


    private void deleteMovie() {

        try {
            getDatabaseHelper().getMovieDao().delete(movie);
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }


}
