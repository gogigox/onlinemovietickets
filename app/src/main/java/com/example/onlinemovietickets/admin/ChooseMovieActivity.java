package com.example.onlinemovietickets.admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.account.BaseActivity;
import com.example.onlinemovietickets.adapter.RVadapterMovieList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;


//povezana je sa xml od activity_base !

public class ChooseMovieActivity extends AppCompatActivity implements RVadapterMovieList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterMovieList rVadapterMovieList;


    private Toolbar toolbar;
    private Button btnAddMovie;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabMyAccount;
    private FloatingActionButton fabLogout;

    private DatabaseHelper databaseHelper;

    private TextView tvFabMyAcc;


    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        fabBack = findViewById(R.id.activity_base_admin_fab);


        fabMyAccount = findViewById(R.id.activity_base_account_fab);
        fabMyAccount.setVisibility(View.GONE);

        tvFabMyAcc = findViewById(R.id.tv_activity_base_fabAcc_text);
        tvFabMyAcc.setVisibility(View.GONE);

        btnAddMovie = findViewById(R.id.btn_add_movie);
        btnAddMovie.setVisibility(View.GONE);



        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChooseMovieActivity.this, AdminProjectionActivity.class);
                startActivity(intent);

            }
        });

        fabLogout = findViewById(R.id.activity_base_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ChooseMovieActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

        setupToolbar();
        setupRV();

    }


    private void setupRV(){
        recyclerView = findViewById(R.id.rv_movie_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }



    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(rVadapterMovieList == null) {

            try {
                rVadapterMovieList = new RVadapterMovieList(getDatabaseHelper().getMovieDao().queryForAll(), this);
                recyclerView.setAdapter(rVadapterMovieList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            rVadapterMovieList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Movie movie) {
        
        Intent intent = new Intent(ChooseMovieActivity.this, ChooseHallActivity.class);
        intent.putExtra("MOVIE_ID",String.valueOf(movie.getId()));
        startActivity(intent);

    }




    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
