package com.example.onlinemovietickets.admin;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;;
import android.widget.TextView;
import android.widget.Toast;


import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.account.EditAccountActivity;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.register.HelpMethods;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


//povezan sa xml od activity_signup !

public class AddAccountActivity extends AppCompatActivity implements View.OnClickListener {


    private DatabaseHelper databaseHelper;

    private EditText etAddAccountName;
    private EditText etAddAccountSurname;
    private EditText etAddAccountBirthDate;
    private EditText etAddAccountPassword;
    private EditText etAddAccountUsername;
    private Button btnAddAccountConfirm;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnSignup;
    private TextView tvLinkLogin;

    private Accounts accounts;

    private HelpMethods hm;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        btnSignup = findViewById(R.id.btn_signup_create_account);
        btnSignup.setVisibility(View.GONE);

        tvLinkLogin = (TextView)findViewById(R.id.link_login);
        tvLinkLogin.setVisibility(View.GONE);


        etAddAccountName = findViewById(R.id.et_name);
        etAddAccountSurname = findViewById(R.id.et_surname);
        etAddAccountBirthDate = findViewById(R.id.et_birthDate);
        etAddAccountPassword = findViewById(R.id.et_signup_password);
        etAddAccountUsername = findViewById(R.id.et_signup_userName);
        btnAddAccountConfirm  = findViewById(R.id.btn_signup_confirm);
        fabBack =  findViewById(R.id.activity_signup_fab);
        fabLogout = findViewById(R.id.activity_signup_fab_logout);

        etAddAccountBirthDate.setOnClickListener(this);

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddAccountActivity.this, AdminAccountActivity.class);
                startActivity(intent);

            }
        });

        btnAddAccountConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add();

            }
        });

        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddAccountActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });



    }


    @Override
    public void onClick(View v) {

        // klasa helpMethods za kreiranje kalendara
        hm = new HelpMethods();
        int[] datumNiz = hm.calendarPosition(etAddAccountBirthDate.getText().toString());
        final int mDay = datumNiz[0];
        final int mMonth = datumNiz[1];
        final int mYear = datumNiz[2];

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        etAddAccountBirthDate.setText(hm.createDateString(dayOfMonth, monthOfYear, year));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }


    private void add() {

        if (!validate()) {
            onSignupFailed();
            return;
        }

        String bDate = (etAddAccountBirthDate.getText().toString().trim());
        String name = etAddAccountName.getText().toString();
        String surname = etAddAccountSurname.getText().toString();
        String password = etAddAccountPassword.getText().toString();
        String userName = etAddAccountUsername.getText().toString();

        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date dateObj = null;

        try {
            dateObj = curFormater.parse(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        accounts = new Accounts();
        accounts.setName(name);
        accounts.setSurname(surname);
        accounts.setBirthDate(dateObj);
        accounts.setPassword(password);
        accounts.setUserName(userName);


        try {
            getDatabaseHelper().getAccountsDao().create(accounts);


        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getCause().getCause() instanceof SQLiteConstraintException){
                Toast.makeText(this, "Username already exists!", Toast.LENGTH_LONG).show();
                return;
            }
        }

        btnAddAccountConfirm.setEnabled(false);

        onSignupSuccess();

    }


    public void onSignupSuccess() {
       btnAddAccountConfirm.setEnabled(true);
        setResult(RESULT_OK, null);
        System.out.println("account added successfully");
        finish();
    }


    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Signup failed", Toast.LENGTH_LONG).show();

        btnAddAccountConfirm.setEnabled(true);

    }


    public boolean validate() {
        boolean valid = true;

        String name = etAddAccountName.getText().toString();
        String surname = etAddAccountSurname.getText().toString();
        String birthDate = etAddAccountBirthDate.getText().toString();
        String password = etAddAccountPassword.getText().toString();
        String userName = etAddAccountUsername.getText().toString();


        if (name.isEmpty() || name.length() < 2) {
            etAddAccountName.setError("at least 2 characters");
            valid = false;
        } else {
            etAddAccountName.setError(null);
        }


        if (surname.isEmpty() || surname.length() < 2) {
            etAddAccountSurname.setError("at least 2 characters");
            valid = false;
        } else {
            etAddAccountSurname.setError(null);
        }


        if (birthDate.isEmpty()) {
            etAddAccountBirthDate.setError("required field");
            valid = false;
        } else {
            etAddAccountBirthDate.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            etAddAccountPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            etAddAccountPassword.setError(null);
        }


        if (userName.isEmpty() || userName.length() < 3) {
            etAddAccountUsername.setError("at least 3 characters");
            valid = false;
        } else {
            etAddAccountUsername.setError(null);
        }

        return valid;

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
