package com.example.onlinemovietickets.register;

import java.util.Calendar;

public class HelpMethods {

    public int[] calendarPosition(String textDatum) {
        int mYear;
        int mMonth;
        int mDay;
        if (textDatum.equalsIgnoreCase("")) {
            Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else{
            String [] parts = textDatum.split("-");
            mDay = Integer.parseInt(parts[0]);
            mMonth = Integer.parseInt(parts[1]) - 1;
            mYear = Integer.parseInt(parts[2]);
        }
        int [] datum = {mDay, mMonth, mYear};
        return datum;

    }

    public String  createDateString(int dayOfMonth, int monthOfYear, int year){
        String datum = "";
        if (dayOfMonth < 10) {
            datum += "0";
        }
        datum += dayOfMonth + "-";
        if (monthOfYear + 1 < 10) {
            datum += "0";
        }
        datum += monthOfYear + 1 + "-";
        datum += year;
        return datum;
    }
}
