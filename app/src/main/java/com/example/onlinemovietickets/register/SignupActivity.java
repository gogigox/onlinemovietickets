package com.example.onlinemovietickets.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.account.BaseActivity;
import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SignupActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "SignupActivity";

    private DatabaseHelper databaseHelper;

    private EditText et_name;
    private EditText et_surName;
    private EditText et_birthDate;
    private EditText et_password;
    private EditText et_userName;
    private Button btn_signup;
    private Button btnConfirm;
    private TextView tv_linkLogin;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private TextView tvLogout;
    private TextView tvBack;

    private Accounts accounts;

    private HelpMethods hm;



    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        btnConfirm = findViewById(R.id.btn_signup_confirm);
        btnConfirm.setVisibility(View.GONE);

        fabBack = findViewById(R.id.activity_signup_fab);
        fabBack.setVisibility(View.GONE);

        fabLogout = findViewById(R.id.activity_signup_fab_logout);
        fabLogout.setVisibility(View.GONE);

        tvLogout = findViewById(R.id.activity_signup_fab_logout_text);
        tvLogout.setVisibility(View.GONE);

        tvBack = findViewById(R.id.activity_signup_fab_back_text);
        tvBack.setVisibility(View.GONE);

        et_name = findViewById(R.id.et_name);
        et_surName = findViewById(R.id.et_surname);
        et_birthDate = findViewById(R.id.et_birthDate);
        et_password = findViewById(R.id.et_signup_password);
        et_userName = findViewById(R.id.et_signup_userName);
        btn_signup = findViewById(R.id.btn_signup_create_account);
        tv_linkLogin = (TextView)findViewById(R.id.link_login);
        tv_linkLogin.getPaint().setUnderlineText(true);

        et_birthDate.setOnClickListener(this);


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signup();
            }
        });


        tv_linkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // zavrsava ekran registracije i vraca na login activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    public void onClick(View v) {

        //help methods za instanciranje calendara
        hm = new HelpMethods();
        int[] datumNiz = hm.calendarPosition(et_birthDate.getText().toString());
        final int mDay = datumNiz[0];
        final int mMonth = datumNiz[1];
        final int mYear = datumNiz[2];

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        et_birthDate.setText(hm.createDateString(dayOfMonth, monthOfYear, year));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }


    public void signup() {
        Log.d(TAG, "Signup");


        if (!validate()) {
            onSignupFailed();
            return;
        }


        String bDate = et_birthDate.getText().toString();
        String name = et_name.getText().toString();
        String surname = et_surName.getText().toString();
        String password = et_password.getText().toString();
        String userName = et_userName.getText().toString();


        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
         Date dateObj = null;

        try {
           dateObj = curFormater.parse(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        accounts = new Accounts();
        accounts.setName(name);
        accounts.setSurname(surname);
        accounts.setBirthDate(dateObj);
        accounts.setPassword(password);
        accounts.setUserName(userName);



        try {
            getDatabaseHelper().getAccountsDao().create(accounts);

            Log.d(TAG, "onClick: ACCOUNT SAVED" + accounts.getName());

        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getCause().getCause() instanceof SQLiteConstraintException){
                Toast.makeText(getBaseContext(), "Username already exists!", Toast.LENGTH_LONG).show();
                return;
            }
        }

        btn_signup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.Theme_AppCompat_Light);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // uspesna registracija
                        onSignupSuccess();
                        // nije uspela registracija
                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    public void onSignupSuccess() {
        btn_signup.setEnabled(true);
        setResult(RESULT_OK, null);
        Intent intent = new Intent(this, BaseActivity.class);
        intent.putExtra("ACCOUNT_ID", String.valueOf(accounts.getId()));
        startActivity(intent);
        finish();
    }


    public void onSignupFailed() {
        Toast.makeText(this, "Signup failed", Toast.LENGTH_LONG).show();

        btn_signup.setEnabled(true);

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    public boolean validate() {
        boolean valid = true;

        String name = et_name.getText().toString();
        String surname = et_surName.getText().toString();
        String birthDate = et_birthDate.getText().toString();
        String password = et_password.getText().toString();
        String userName = et_userName.getText().toString();


        if (name.isEmpty() || name.length() < 2) {
            et_name.setError("at least 2 characters");
            valid = false;
        } else {
            et_name.setError(null);
        }


        if (surname.isEmpty() || surname.length() < 2) {
            et_surName.setError("at least 2 characters");
            valid = false;
        } else {
            et_surName.setError(null);
        }


        if (birthDate.isEmpty()) {
            et_birthDate.setError("required field");
            valid = false;
        } else {
            et_birthDate.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            et_password.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            et_password.setError(null);
        }


        if (userName.isEmpty() || userName.length() < 3) {
            et_userName.setError("at least 3 characters");
            valid = false;
        } else {
            et_userName.setError(null);
        }

        return valid;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
