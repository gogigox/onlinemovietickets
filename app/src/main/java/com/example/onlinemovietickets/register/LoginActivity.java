package com.example.onlinemovietickets.register;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.account.BaseActivity;
import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.admin.AdminActivity;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;

import java.sql.SQLException;


public class LoginActivity extends AppCompatActivity {

    static Dao<Accounts, Integer> accountsDao;

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private EditText et_userName;
    private EditText et_password;
    private Button btn_login;
    private TextView tv_linkSignup;

    private Accounts accounts;

    private DatabaseHelper databaseHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        et_userName = findViewById(R.id.et_userName);
        et_password = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        tv_linkSignup = (TextView) findViewById(R.id.link_signup);
        tv_linkSignup.getPaint().setUnderlineText(true);



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


        tv_linkSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
            }
        });

    }



    public void login() {
        Log.d(TAG, "Login");


        String username = et_userName.getText().toString();
        String password = et_password.getText().toString();

        try {
            accountsDao = getDatabaseHelper().getAccountsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }



        //pravimo upit nad bazom i proveramo da li se username i password poklapauju sa nekim
        //iz baze, ako je to ok moze da se uloguje, inace je failed
        QueryBuilder<Accounts, Integer> queryBuilder =
                accountsDao.queryBuilder();
        Where<Accounts, Integer> where = queryBuilder.where();
        SelectArg selectArg = new SelectArg();

        try {
            where.eq("username", selectArg);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        PreparedQuery<Accounts> preparedQuery = null;
        try {
            preparedQuery = queryBuilder.prepare();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        selectArg.setValue(username);
        try {
            accounts = accountsDao.queryForFirst(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if (accounts != null && accounts.getPassword().equals(password)) {
            if (accounts.getIsAdmin() == 0) {
                onLoginSuccess();
            } else if (accounts.getIsAdmin() == 1) {
                loginAdmin();
            }


        } else {
            onLoginFailed();
            return;

        }
        btn_login.setEnabled(false);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                //По дефаулту само завршили Активност и аутоматски се пријавимо
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // онемогућити повратак на main activity
        moveTaskToBack(true);
    }


    public void onLoginSuccess() {
        btn_login.setEnabled(true);


        Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
        intent.putExtra("ACCOUNT_ID", String.valueOf(accounts.getId()));
        startActivity(intent);
        finish();
    }


    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        btn_login.setEnabled(true);
    }


    public void loginAdmin() {
        btn_login.setEnabled(true);
        Intent intent = new Intent(getApplicationContext(), AdminActivity.class);
        startActivity(intent);
        finish();

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
