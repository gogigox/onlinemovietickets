package com.example.onlinemovietickets.account;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;


public class MyAccountActivity extends AppCompatActivity {


    private DatabaseHelper databaseHelper;

    private Button btnEditAccount;
    private Button btnDeleteAccount;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private Toolbar toolbar;

    private Accounts accounts;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);


        btnEditAccount = findViewById(R.id.btn_edit_account);
        btnDeleteAccount = findViewById(R.id.btn_delete_account);
        fabBack = findViewById(R.id.activity_my_account_fab);
        fabLogout = findViewById(R.id.activity_my_account_fab_logout);


        btnEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyAccountActivity.this, EditAccountActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });


        btnDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callDialog();

            }
        });


        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyAccountActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });

        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyAccountActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        setupToolbar();

    }


    private void callDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("are you sure you want to permanently delete your account?");
        AlertDialog dialog = builder.create();
        dialog.setButton(Dialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }


                });
        dialog.setButton(Dialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        deleteAccount();

                        Toast.makeText(MyAccountActivity.this, "account deleted", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(MyAccountActivity.this, LoginActivity.class);
                        startActivity(intent);

                    }


                });
        dialog.show();

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }



    private void deleteAccount() {

        int accountId = Integer.parseInt(getIntent().getStringExtra("ACCOUNT_ID"));

        try {
            accounts = getDatabaseHelper().getAccountsDao().queryForId(accountId);

            getDatabaseHelper().getAccountsDao().delete(accounts);

        } catch (SQLException e) {
            e.printStackTrace();

        }
    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }

}
