package com.example.onlinemovietickets.account;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterMovieList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


//prva activity koju korisnik vidi posle login/ signup

public class BaseActivity extends AppCompatActivity implements RVadapterMovieList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterMovieList rVadapterMovieList;

    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    List<String> drawerItems;
    ListView drawerList;
    private FloatingActionButton fabMyAccount;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnAddMovie;
    private TextView tvFabBack;


    private DatabaseHelper databaseHelper;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        btnAddMovie = findViewById(R.id.btn_add_movie);
        btnAddMovie.setVisibility(View.GONE);

        tvFabBack = findViewById(R.id.activity_base_fabBack_text);
        tvFabBack.setVisibility(View.GONE);


        fabBack = findViewById(R.id.activity_base_admin_fab);
        fabBack.setVisibility(View.GONE);

        fabMyAccount = findViewById(R.id.activity_base_account_fab);

        fabLogout = findViewById(R.id.activity_base_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BaseActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        setTitle("movie tickets");
        fillData();
        setupToolbar();
        setupDrawer();
        setupRV();


        fabMyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BaseActivity.this, MyAccountActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);      // home button otvara left drawer
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }

    }


    private void fillData() {
        drawerItems = new ArrayList<>();
        drawerItems.add("History");
        drawerItems.add("Coming soon");
        drawerItems.add("My account");
    }


    private void setupDrawer() {
        drawerList = findViewById(R.id.left_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, drawerItems) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorText));  //setuju se slova u left draweru
                return textView;                                                                // da su bele boje
            }
        });
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = "Unknown";
                switch (i) {
                    case 0:
                        title = "history";
                        Intent intent = new Intent(BaseActivity.this, HistoryActivity.class);
                        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                        startActivity(intent);
                        break;
                    case 1:
                        title = "coming soon";
                        intent = new Intent(BaseActivity.this, ComingSoonActivity.class);
                        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                        startActivity(intent);
                        break;
                    case 2:
                        title = "my account";
                        intent = new Intent(BaseActivity.this, MyAccountActivity.class);
                        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                        startActivity(intent);
                        break;
                    default:
                        break;
                }
                //drawerList.setItemChecked(i, true);
                setTitle(title);
                drawerLayout.closeDrawer(drawerList);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawerLayout,                   /* DrawerLayout object */
                toolbar,                        /* nav drawer image to replace 'Up' caret */
                R.string.app_name,           /* "open drawer" description for accessibility */
                R.string.app_name           /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }
        };
    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_movie_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterMovieList == null) {

            try {

                //u ovoj activity treba da prikazuje samo one filmove koji imaju projekcije(filmove bez projekcije ne treba)
                //to se radi tako sto se sa inner join radi presek tabela movie i projection
                //dobiju se rawResults koje stavimo u movieList i to setujemo u adapter


                String query = "SELECT DISTINCT m.title, m.genre, m.actors, m.image, m.id FROM Movie m INNER JOIN Projection p ON m.id = p.movie";
                GenericRawResults<Movie> rawResults =
                        getDatabaseHelper().getMovieDao().queryRaw(query, getDatabaseHelper().getMovieDao().getRawRowMapper());
                List<Movie> movieList = rawResults.getResults();
                rVadapterMovieList = new RVadapterMovieList(movieList, this);
                recyclerView.setAdapter(rVadapterMovieList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterMovieList.notifyDataSetChanged();
        }
    }

    @Override
    public void onRVItemclick(Movie movie) {

        Intent intent = new Intent(BaseActivity.this, MovieDetailActivity.class);
        intent.putExtra("MOVIE_ID", String.valueOf(movie.getId()));
        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
        startActivity(intent);

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
