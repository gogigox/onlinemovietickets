package com.example.onlinemovietickets.account;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.register.HelpMethods;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class EditAccountActivity extends AppCompatActivity implements View.OnClickListener {


    private DatabaseHelper databaseHelper;

    private EditText etEditName;
    private EditText etEditSurname;
    private EditText etEditBirthDate;
    private EditText etEditPassword;
    private EditText etEditUsername;
    private int accountId;
    private Button btnEditAccount;
    private Button btnConfirm;
    private TextView tv_linkLogin;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;


    private Accounts accounts;

    private HelpMethods hm;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        btnConfirm = findViewById(R.id.btn_signup_confirm);
        btnConfirm.setVisibility(View.GONE);

        tv_linkLogin = (TextView)findViewById(R.id.link_login);
        tv_linkLogin.setVisibility(View.GONE);


        etEditName = findViewById(R.id.et_name);
        etEditSurname = findViewById(R.id.et_surname);
        etEditBirthDate = findViewById(R.id.et_birthDate);
        etEditPassword= findViewById(R.id.et_signup_password);
        etEditUsername = findViewById(R.id.et_signup_userName);
        btnEditAccount = findViewById(R.id.btn_signup_create_account);
        fabBack = findViewById(R.id.activity_signup_fab);
        fabLogout = findViewById(R.id.activity_signup_fab_logout);


        // listener kojim se bira datum
        etEditBirthDate.setOnClickListener(this);


        checkAndSet(getIntent());

        btnEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });

        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditAccountActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });

        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditAccountActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


    }


    private void checkAndSet(Intent intent) {

        // prosledjuje se accountov id
        accountId = Integer.parseInt(getIntent().getStringExtra("ACCOUNT_ID"));

        if(accountId < 0){
            Toast.makeText(this, " Error! accountId " + accountId + " ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            accounts = getDatabaseHelper().getAccountsDao().queryForId(accountId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        etEditName.setText(accounts.getName());
        etEditSurname.setText(accounts.getSurname());

        String bDate = etEditBirthDate.getText().toString();

        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date dateObj = accounts.getBirthDate();

        try {
            dateObj = curFormater.parse(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar c = Calendar.getInstance();
        c.setTime(dateObj);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH) + 1;
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        String date = String.valueOf(mDay) + '-' + String.valueOf(mMonth) + '-' + String.valueOf(mYear);


        etEditBirthDate.setText(date);
        etEditPassword.setText(accounts.getPassword());
        etEditUsername.setText(accounts.getUserName());
        etEditUsername.setFocusable(false);

    }


    @Override
    public void onClick(View v) {

       // klasa help methods sluzi za postavljanje kalendara
        hm = new HelpMethods();
        int[] datumNiz = hm.calendarPosition(etEditBirthDate.getText().toString());
        final int mDay = datumNiz[0];
        final int mMonth = datumNiz[1];
        final int mYear = datumNiz[2];

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        etEditBirthDate.setText(hm.createDateString(dayOfMonth, monthOfYear, year));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    private void saveChanges() {

        if (!validate()) {
            onSaveChangesFailed();
            return;
        }

        String bDate = etEditBirthDate.getText().toString();
        String name = etEditName.getText().toString();
        String surname = etEditSurname.getText().toString();
        String password = etEditPassword.getText().toString();
        String userName = etEditUsername.getText().toString();

        SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
        Date dateObj = null;

        try {
            dateObj = curFormater.parse(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        accounts.setName(name);
        accounts.setSurname(surname);
        accounts.setBirthDate(dateObj);
        accounts.setPassword(password);
        accounts.setUserName(userName);


        try {
            getDatabaseHelper().getAccountsDao().createOrUpdate(accounts);
            onSaveChangesSuccess();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void onSaveChangesSuccess() {
        btnEditAccount.setEnabled(true);
        setResult(RESULT_OK, null);
        Toast.makeText(this, "changes  saved !", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
        startActivity(intent);
        finish();
    }


    public void onSaveChangesFailed() {
        Toast.makeText(this, "changes not saved !", Toast.LENGTH_LONG).show();

        btnEditAccount.setEnabled(true);

    }



    public boolean validate() {
        boolean valid = true;

        String name = etEditName.getText().toString();
        String surname = etEditSurname.getText().toString();
        String birthDate = etEditBirthDate.getText().toString();
        String password = etEditPassword.getText().toString();
        String userName = etEditUsername.getText().toString();


        if (name.isEmpty() || name.length() < 2) {
            etEditName.setError("at least 2 characters");
            valid = false;
        } else {
            etEditName.setError(null);
        }


        if (surname.isEmpty() || surname.length() < 2) {
            etEditSurname.setError("at least 2 characters");
            valid = false;
        } else {
            etEditSurname.setError(null);
        }


        if (birthDate.isEmpty()) {
            etEditBirthDate.setError("required field");
            valid = false;
        } else {
            etEditBirthDate.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            etEditPassword.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            etEditPassword.setError(null);
        }


        if (userName.isEmpty() || userName.length() < 3) {
            etEditUsername.setError("at least 3 characters");
            valid = false;
        } else {
            etEditUsername.setError(null);
        }

        return valid;

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
