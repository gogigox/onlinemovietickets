package com.example.onlinemovietickets.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.db.Accounts;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.db.Ticket;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;


import java.sql.SQLException;
import java.util.Calendar;


import static android.widget.Toast.LENGTH_LONG;


public class BuyTicketActivity extends AppCompatActivity {


    static Dao<Ticket, Integer> ticketDao;

    private DatabaseHelper databaseHelper;


    private TextView tvProjectionDetailTitle;
    private TextView tvMovie;
    private TextView tvTime;
    private TextView tvHall;
    private TextView tvTicketPrice;
    private TextView tvNumOfTickets;
    private EditText etNumOfTickets;
    private TextView tvTotalPrice;
    private EditText etTotalPrice;

    private Movie movie;

    private Projection projection;

    private Ticket ticket;

    private Accounts accounts;

    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projection_detail);

        tvProjectionDetailTitle = findViewById(R.id.tv_projection_detail_title);
        tvProjectionDetailTitle.setVisibility(View.GONE);


        btnDelete = findViewById(R.id.btn_projection_delete);
        btnDelete.setText("confirm");
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                countTickets();


            }
        });

        fabBack = findViewById(R.id.activity_projection_detail_fab);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                movie = projection.getMovie();
                Intent intent = new Intent(BuyTicketActivity.this, MovieDetailActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                intent.putExtra("MOVIE_ID", String.valueOf(movie.getId()));
                startActivity(intent);
            }
        });

        fabLogout = findViewById(R.id.activity_projection_detail_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BuyTicketActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        getAndSetData(getIntent());

    }


    private void getAndSetData(Intent intent) {

        //setuje podatke- projekcijin id

        int projection_id = Integer.parseInt(getIntent().getStringExtra("PROJECTION_ID"));

        try {
            projection = getDatabaseHelper().getProjectionDao().queryForId(projection_id);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        tvMovie = findViewById(R.id.tv_projection_detail_movie);
        tvTime = findViewById(R.id.tv_projection_detail_time);
        tvHall = findViewById(R.id.tv_projection_detail_hall);
        tvTicketPrice = findViewById(R.id.tv_projection_detail_ticket_price);
        tvNumOfTickets = findViewById(R.id.tv_num_of_tickets);
        etNumOfTickets = findViewById(R.id.et_num_of_tickets);
        tvTotalPrice = findViewById(R.id.tv_total_price);
        etTotalPrice = findViewById(R.id.et_total_price);
        etTotalPrice.setFocusable(false);


        //listener koji sluzi da odmah prikaze ukupnu cenu karata, kada korisink ukuca broj karata

        etNumOfTickets.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                etTotalPrice.setFocusable(false);

                String sTicketPrice = tvTicketPrice.getText().toString();
                int intTicketPrice = Integer.parseInt(sTicketPrice);

                int totalPrice;

                String sNumberOfTickets = etNumOfTickets.getText().toString();
                int intNumberOfTickets = 0;
                try {
                    intNumberOfTickets = Integer.parseInt(sNumberOfTickets);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }


                totalPrice = intTicketPrice * intNumberOfTickets;
                String sTotalPrice = String.valueOf(totalPrice);

                etTotalPrice.setText(sTotalPrice + " din");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        // calendar koji sluzi za prikazivanje datuma i vremena projekcije- da ispred min i sata bude 0 ako su <= 9
        // i za mesec dodaje 1 jer bi inace brojao mesece od 0, a Calendar.DATE = Calendar.DAY_OF_MONTH

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(projection.getTime());


        int min = calendar.get(Calendar.MINUTE);
        int hrs = calendar.get(Calendar.HOUR_OF_DAY);
        int month = calendar.get(Calendar.MONTH) + 1;

        String monthStr = String.valueOf(month);
        if (month <= 9) {
            monthStr = 0 + monthStr;
        }

        String minutes = "";
        String hours = "";
        if (min <= 9) {
            minutes = '0' + String.valueOf(min);
        } else {
            minutes = String.valueOf(min);
        }
        if (hrs <= 9) {
            hours = '0' + String.valueOf(hrs);
        } else {
            hours = String.valueOf(hrs);
        }
        String datum = calendar.get(Calendar.DATE) + "-" + monthStr + "-" + calendar.get(Calendar.YEAR)
                + " " + hours + ":" + minutes;


        tvMovie.setText(projection.getMovie().getTitle());
        tvTime.setText(datum);
        tvHall.setText(projection.getHall().getNumber());
        tvTicketPrice.setText(Integer.toString(projection.getTicketPrice()));

    }


    // metoda za proveru da li korisnik moze da kupi zeljeni br karata
    private void countTickets() {

        try {

            // gledamo ukupan br sedista-to je br sedista koji ima sala
            int totalNumOfSeats = Integer.parseInt(projection.getHall().getNumberOfSeats());

            // treba nam dao objekat da bi iz baze pozivali prodate karte
            ticketDao = getDatabaseHelper().getTicketDao();

            // metoda countOf ce nam dati br koliko karata je vec prodato za tu projekciju-queri upit nad bazom
            // i trazi kljuc-vrednost parove odnosno trazi iz baze karata sve karte koje u koloni "projection" imaju
            // istu konkretnu vrednost - projection, odnosno pripadaju istoj projekciji
            long soldTickets = ticketDao.queryBuilder().where().eq("projection", projection).countOf();

            // ukupan br karata-kupljene = preosatale karte
            int restTickets = (int) (totalNumOfSeats - soldTickets);

            // ovo je br karata koje je korisnik ukucao, parsiramo u int
            String sNumberOfTickets = etNumOfTickets.getText().toString();
            int intNumberOfTickets = Integer.parseInt(sNumberOfTickets);

            // ako ima vise preostalih nego sto korisnik zeli da kupi, teba da kreira onoliko karata koliko
            // je korisnik napisao, tj. toliko puta se poziva metoda createTicket();

            if (intNumberOfTickets == 0) {
                Toast.makeText(this, "0 Tickets selected !", LENGTH_LONG).show();


            } else if (restTickets >= intNumberOfTickets) {

                for (int i = 0; i < intNumberOfTickets; i++) {
                    createTicket();
                }

                Toast.makeText(this, "you bought  " + intNumberOfTickets +
                        "  tickets for  " + projection.getMovie().getTitle(), LENGTH_LONG).show();

                Intent intent = new Intent(BuyTicketActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            } else if (restTickets == 0) {
                Toast.makeText(this, "no tickets  ", LENGTH_LONG).show();

            } else {

                callDialog();
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void createTicket() {

        // potreban accauntId da bi tiket bio povezan sa accountom
        int accountId = Integer.parseInt(getIntent().getStringExtra("ACCOUNT_ID"));

        try {
            accounts = getDatabaseHelper().getAccountsDao().queryForId(accountId);

            ticketDao = getDatabaseHelper().getTicketDao();

            // dobijemo br karata koje su prodane za jednu konkretnu projekciju
            // i kad kreiramo ticket vezujemo ih sa konkretnim accountom
            // i svaka karta ce imati br sedista za 1 veci od prethodne, zato ide +1
            long soldTickets = ticketDao.queryBuilder().where().eq("projection", projection).countOf();


            ticket = new Ticket();
            ticket.setAccounts(accounts);
            ticket.setProjection(projection);
            ticket.setSeatNumber((int) (soldTickets + 1));


            getDatabaseHelper().getTicketDao().create(ticket);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    // metoda koja se pozivaako je zeljeni br karata > od preostalih
    private void callDialog() {

        // ovi podaci su isti kao u metodi countTickets();
        int projection_id = Integer.parseInt(getIntent().getStringExtra("PROJECTION_ID"));

        try {
            projection = getDatabaseHelper().getProjectionDao().queryForId(projection_id);

            int totalNumOfSeats = Integer.parseInt(projection.getHall().getNumberOfSeats());

            ticketDao = getDatabaseHelper().getTicketDao();

            long soldTickets = ticketDao.queryBuilder().where().eq("projection", projection).countOf();

            final int restTickets = (int) (totalNumOfSeats - soldTickets);


            // kreira se alert dialog koji kaze koliko je karata preostalo, ako korisnik pristane da ih kupi
            // isto se poziva metoda createTicket za taj br karata
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("there is only " + restTickets + " tickets left " +
                    "do you want to buy them?");
            AlertDialog dialog = builder.create();
            dialog.setButton(Dialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }


                    });
            dialog.setButton(Dialog.BUTTON_POSITIVE, "YES",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            for (int i = 0; i < restTickets; i++) {
                                createTicket();
                            }
                            Toast.makeText(getApplicationContext(), "you bought  " + restTickets +
                                    "  tickets for  " + projection.getMovie().getTitle(), LENGTH_LONG).show();

                            Intent intent = new Intent(BuyTicketActivity.this, BaseActivity.class);
                            intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                            startActivity(intent);

                        }

                    });
            dialog.show();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }

}
