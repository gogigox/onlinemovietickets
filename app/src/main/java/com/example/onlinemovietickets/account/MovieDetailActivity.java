package com.example.onlinemovietickets.account;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterProjectionList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.db.Projection;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MovieDetailActivity extends AppCompatActivity implements RVadapterProjectionList.OnRVItemClick{


    private RecyclerView recyclerView;

    private RVadapterProjectionList rVadapterProjectionList;

    private DatabaseHelper databaseHelper;

    private TextView tvAddMovie;
    private ImageButton ibMovieDetail;
    private TextView tvTitleDetail;
    private TextView tvGenreDetail;
    private TextView tvActorsDetail;
    private Spinner spMovieGenre;

    private Movie movie;


    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnDelete;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);


        spMovieGenre = findViewById(R.id.spinner);
        spMovieGenre.setVisibility(View.GONE);

        tvAddMovie = findViewById(R.id.tv_add_movie);
        tvAddMovie.setVisibility(View.GONE);


        getAndSetData(getIntent());
        setupRV();

        btnDelete = findViewById(R.id.btn_add_movie_confirm);
        btnDelete.setVisibility(View.GONE);

        fabBack = findViewById(R.id.activity_add_movie_fab_back);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MovieDetailActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);
            }
        });

        fabLogout = findViewById(R.id.activity_add_movie_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MovieDetailActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }

    private void getAndSetData(Intent intent) {

        // id odredjenog filma
        int movie_id = Integer.parseInt(getIntent().getStringExtra("MOVIE_ID"));


        try {
            movie = getDatabaseHelper().getMovieDao().queryForId(movie_id);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tvTitleDetail = findViewById(R.id.et_add_movie_title);
        tvTitleDetail.setFocusable(false);
        tvGenreDetail = findViewById(R.id.et_add_movie_genre);
        tvGenreDetail.setFocusable(false);
        tvActorsDetail = findViewById(R.id.et_add_movie_actors);
        tvActorsDetail.setFocusable(false);
        ibMovieDetail = findViewById(R.id.ib_add_movie);
        ibMovieDetail.setFocusable(false);
        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) ibMovieDetail.getLayoutParams();
        marginParams.setMargins(marginParams.leftMargin,
                80,
                marginParams.rightMargin,      //namesta se ibMovieDetail jer se ovde ne vidi tvAddMovie
                marginParams.bottomMargin);

        tvTitleDetail.setText(movie.getTitle());
        tvGenreDetail.setText(movie.getGenre());
        tvActorsDetail.setText(movie.getActors());
        ibMovieDetail.setImageBitmap(BitmapFactory.decodeFile(movie.getImage()));

    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_projections_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterProjectionList == null) {

            int movie_id = Integer.parseInt(getIntent().getStringExtra("MOVIE_ID"));



            try {

                Date currentDate = new Date();
                Calendar cal = Calendar.getInstance(); // kreira calendar posto je new Date ide od sadasnjeg trenutka
                cal.setTime(currentDate);

                int month = cal.get(Calendar.MONTH) + 1;  // mesec ide od 0 zato je + 1
                String monthStr = String.valueOf(month);
                if(month <= 9){
                    monthStr = 0 + monthStr;
                }

                int day = cal.get(Calendar.DAY_OF_MONTH);
                String dayStr = String.valueOf(day);
                if(day <= 9){
                    dayStr = 0 + dayStr;
                }

                int min = cal.get(Calendar.MINUTE);
                String minStr = String.valueOf(min);
                if(min <= 9){
                    minStr = 0 + minStr;
                }

                int hrs = cal.get(Calendar.HOUR_OF_DAY);
                String hours = "";
                if (hrs <= 9) {
                    hours = '0' + String.valueOf(hrs);
                } else {
                    hours = String.valueOf(hrs);
                }

                // pravi se upit nad bazom gde se traze sve projekcije koje su >= od sadasnjeg trenutka
                // dobije se rawResults koji daje listu koju setujemo u adapter
                String forBase = "'" + cal.get(Calendar.YEAR) + '-' + monthStr +
                        '-' + dayStr + " " + hours + ':' + minStr + ":00" + "'";
                String query = "SELECT * FROM Projection WHERE Datetime(time) >= " + forBase + " AND movie = " + movie_id + " ORDER BY time";
                GenericRawResults<Projection> rawResults =
                        getDatabaseHelper().getProjectionDao().queryRaw(query, getDatabaseHelper().getProjectionDao().getRawRowMapper());
                List<Projection> listaP = rawResults.getResults();

                rVadapterProjectionList = new RVadapterProjectionList(listaP, this);
                recyclerView.setAdapter(rVadapterProjectionList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterProjectionList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Projection projection) {

        Intent intent = new Intent(MovieDetailActivity.this, BuyTicketActivity.class);
        intent.putExtra("PROJECTION_ID",String.valueOf(projection.getId()));
        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
        startActivity(intent);

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
