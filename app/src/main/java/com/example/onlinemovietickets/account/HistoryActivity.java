package com.example.onlinemovietickets.account;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterTicketList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Ticket;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;


public class HistoryActivity extends AppCompatActivity implements RVadapterTicketList.OnRVItemClick {


    private RecyclerView recyclerView;

    private RVadapterTicketList rVadapterTicketList;

    private Toolbar toolbar;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;

    private DatabaseHelper databaseHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        fabBack = findViewById(R.id.activity_history_fab);
        fabLogout = findViewById(R.id.activity_history_fab_logout);

        setupToolbar();
        setupRV();


        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HistoryActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });

        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(HistoryActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_tickets_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterTicketList == null) {

            // prosledjuje se accountov id
            String accountId = String.valueOf(Integer.parseInt(getIntent().getStringExtra("ACCOUNT_ID")));

            // calendar- za prikazivanje liste tiketa koji nisu stariji od godinu dana, zato ide year-1 (mesec je +1 jer krece od 0)
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -1);
            int month = cal.get(Calendar.MONTH) + 1;

            String monthStr = String.valueOf(month);
            if (month <= 9) {
                monthStr = 0 + monthStr;
            }

            int date = cal.get(Calendar.DATE);    // Calendar.DATE = Calendar.DAY_OF_MONTH
            String dateStr = String.valueOf(date);
            if (date <= 9) {
                dateStr = 0 + dateStr;
            }

            // dobije se string datum god dana u proslost
            String pastDate = "'" + cal.get(Calendar.YEAR) + "-" + monthStr + "-" + dateStr + "'";

            // queri nad bazom gde se traze tiketi odredjene projekcije konkretnog accounta i vreme projekcije da nije starije od god dana
            // dobije se lista tiketa i setuje u adapter
            try {
                String query = "SELECT t.id, t.accounts, t.projection, t.seatNumber FROM Ticket t, Projection p WHERE t.projection = p.id " +
                        "and t.accounts =" + accountId + " and p.time > " + pastDate ;
                GenericRawResults<Ticket> rawResults =
                        getDatabaseHelper().getTicketDao().queryRaw(query, getDatabaseHelper().getTicketDao().getRawRowMapper());
                List<Ticket> listaT = rawResults.getResults();
                rVadapterTicketList = new RVadapterTicketList(listaT, this);
                recyclerView.setAdapter(rVadapterTicketList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterTicketList.notifyDataSetChanged();
        }
    }


    @Override
    public void onRVItemclick(Ticket ticket) {

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
