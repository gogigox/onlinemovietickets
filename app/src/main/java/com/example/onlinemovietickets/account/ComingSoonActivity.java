package com.example.onlinemovietickets.account;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.example.onlinemovietickets.R;
import com.example.onlinemovietickets.adapter.RVadapterMovieList;
import com.example.onlinemovietickets.db.DatabaseHelper;
import com.example.onlinemovietickets.db.Movie;
import com.example.onlinemovietickets.register.LoginActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.GenericRawResults;

import java.sql.SQLException;
import java.util.List;



public class ComingSoonActivity extends AppCompatActivity implements RVadapterMovieList.OnRVItemClick {



    private RecyclerView recyclerView;

    private RVadapterMovieList rVadapterMovieList;

    private Toolbar toolbar;

    private FloatingActionButton fabMyAccount;
    private FloatingActionButton fabBack;
    private FloatingActionButton fabLogout;
    private Button btnAddMovie;
    private TextView tvFabAccText;

    private DatabaseHelper databaseHelper;


    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        btnAddMovie = findViewById(R.id.btn_add_movie);
        btnAddMovie.setVisibility(View.GONE);

        fabMyAccount = findViewById(R.id.activity_base_account_fab);
        fabMyAccount.setVisibility(View.GONE);

        tvFabAccText = findViewById(R.id.tv_activity_base_fabAcc_text);
        tvFabAccText.setVisibility(View.GONE);


        fabLogout = findViewById(R.id.activity_base_fab_logout);
        fabLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ComingSoonActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

        fabBack = findViewById(R.id.activity_base_admin_fab);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ComingSoonActivity.this, BaseActivity.class);
                intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
                startActivity(intent);

            }
        });

        setupToolbar();
        setupRV();

    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    private void setupRV() {
        recyclerView = findViewById(R.id.rv_movie_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }


    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (rVadapterMovieList == null) {

            try {

                // seluktuje sa left join samo one filmove koji nemaju projekcije, odnosno koji ce uskoro doci
                // to su filmovi kojih nema u tabeli projekcije, gde je movie kolona u tab projection = null
                // sa rawResults dobijemo comingSoonList koju setujemo u adapter

                String query = "SELECT m.title, m.id FROM Movie m LEFT JOIN Projection p ON m.id = p.movie WHERE movie IS NULL";
                GenericRawResults<Movie> rawResults =
                        getDatabaseHelper().getMovieDao().queryRaw(query, getDatabaseHelper().getMovieDao().getRawRowMapper());
                List<Movie> comingSoonList = rawResults.getResults();
                rVadapterMovieList = new RVadapterMovieList(comingSoonList, this);
                recyclerView.setAdapter(rVadapterMovieList);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            rVadapterMovieList.notifyDataSetChanged();
        }
    }

    @Override
    public void onRVItemclick(Movie movie) {

        Intent intent = new Intent(ComingSoonActivity.this, MovieDetailActivity.class);
        intent.putExtra("MOVIE_ID", String.valueOf(movie.getId()));
        intent.putExtra("ACCOUNT_ID", getIntent().getStringExtra("ACCOUNT_ID"));
        startActivity(intent);

    }


    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }


}
